//app.js
App({
  onLaunch: function () {
    if (wx.canIUse('getUpdateManager')) {
      const updateManager = wx.getUpdateManager()
      updateManager.onCheckForUpdate(function (res) {
        // 请求完新版本信息的回调
        if (res.hasUpdate) {
          console.log('res.hasUpdate====')
          updateManager.onUpdateReady(function () {
            wx.showModal({
              title: '更新提示',
              content: '新版本已经准备好，是否重启应用？',
              success: function (res) {
                console.log('success====', res)
                // res: {errMsg: "showModal: ok", cancel: false, confirm: true}
                if (res.confirm) {
                  // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                  updateManager.applyUpdate()
                }
              }
            })
          })
          updateManager.onUpdateFailed(function () {
            // 新的版本下载失败
            wx.showModal({
              title: '已经有新版本了哟~',
              content: '新版本已经上线啦~，请您删除当前小程序，重新搜索打开哟~'
            })
          })
        }
      })
    }
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
    } else {
      wx.cloud.init({
        env: 'yyxx-hsm2',
        traceUser: true,
      })
    }

    this.globalData = {
      mydata:{
        serviceman:'点击选择',
        name:'',
        address:''
      }
    }
    // 调用云函数
    wx.cloud.callFunction({
      name: 'login', //函数名称
      data: {},
      success: res => {
        console.log('[云函数] [login] user openid: ', res.result.openid)
        this.globalData.openid = res.result.openid // 将openID记录在app.globalData里面，可以全局调用
      },
      fail: err => {
        console.error('[云函数] [login] 调用失败', err)

      }
    })
  },
})


// {
//   "pagePath": "pages/volunteer/task",
//   "text": "志愿活动",
//   "iconPath": "images/vo1.png",
//   "selectedIconPath": "images/vo2.png"
// },

// "pages/volunteer/task",
// "pages/volunteer/task1",
// "pages/volunteer/tasklist",
// "pages/volunteer/taskchange",
// "pages/volunteer/fabu",