// 已移除该模块
var city = [{
    title: "现部长及副部",
    type: 'hot',
    item: [

      {
        "name": "温进鹏",
        "key": "热门",
        "test": "testValue"
      },
      {
        "name": "徐智强",
        "key": "热门"
      },
      {
        "name": "陈逸",
        "key": "热门"
      },
      {
        "name": "冯钰阳",
        "key": "热门"
      }
    ]
  },
  // {
  //   title: "B",
  //   item: [{
  //     "name": "北京",
  //     "key": "B"
  //   },
  //   {
  //     "name": "白银",
  //     "key": "B"
  //   },
  //   {
  //     "name": "保定",
  //     "key": "B"
  //   }
  //   ]
  // },
  {
    title: "C",
    item: [{
        "name": "陈逸",
        "key": "C"
      },
      {
        "name": "陈邵阳",
        "key": "C"
      },
      {
        "name": "程瑞",
        "key": "C"
      },
      {
        "name": "楚香凌",
        "key": "C"
      },
      {
        "name": "陈晶晶",
        "key": "C"
      },
      {
        "name": "陈娴",
        "key": "C"
      },
      {
        "name": "陈浩",
        "key": "C"
      },
      {
        "name": "陈莹莹",
        "key": "C"
      },
      {
        "name": "褚玉泉",
        "key": "C"
      }
    ]
  },
  // {
  //   title: "D",
  //   item: [{
  //     "name": "大连",
  //     "key": "D"
  //   },
  //   {
  //     "name": "东莞",
  //     "key": "D"
  //   },
  //   {
  //     "name": "大理",
  //     "key": "D"
  //   },
  //   {
  //     "name": "丹东",
  //     "key": "D"
  //   }
  //   ]
  // },
  // {
  //   title: "E",
  //   item: [{
  //     "name": "鄂尔多斯",
  //     "key": "E"
  //   },
  //   {
  //     "name": "恩施",
  //     "key": "E"
  //   },
  //   {
  //     "name": "鄂州",
  //     "key": "E"
  //   }
  //   ]
  // },
  {
    title: "F",
    item: [{
        "name": "方徐鑫",
        "key": "F"
      },
      {
        "name": "方狄庆",
        "key": "F"
      }
    ]
  },
  {
    title: "G",
    item: [{
        "name": "高洁",
        "key": "G"
      },
      {
        "name": "顾展豪",
        "key": "G"
      },
      {
        "name": "高呈贞",
        "key": "G"
      }
    ]
  },
  {
    title: "H",
    item: [{
        "name": "何灵燕",
        "key": "H"
      },
      {
        "name": "胡俊玮",
        "key": "H"
      },
      {
        "name": "胡灵芳",
        "key": "H"
      },
      {
        "name": "黄埔",
        "key": "H"
      }
    ]
  },
  {
    title: "J",
    item: [

      {
        "name": "计天鑫",
        "key": "J"
      },
      {
        "name": "金壮",
        "key": "J"
      },
      {
        "name": "金琳超",
        "key": "J"
      }
    ]
  },
  // {
  //   title: "K",
  //   item: [{
  //     "name": "昆明",
  //     "key": "K"

  //   },
  //   {

  //     "name": "开封",
  //     "key": "K"
  //   }
  //   ]
  // },
  {
    title: "L",
    item: [{
        "name": "李恒",
        "key": "L"
      },
      {
        "name": "李楠",
        "key": "L"
      },
      {
        "name": "楼民健",
        "key": "L"
      },
      {
        "name": "林蒙康",
        "key": "L"
      },
      {
        "name": "刘韩琪",
        "key": "L"
      },
      {
        "name": "刘婕",
        "key": "L"
      },
      {
        "name": "吕煜",
        "key": "L"
      },
      {
        "name": "林国鹏",
        "key": "L"
      },
      {
        "name": "刘仁豪",
        "key": "L"
      },
      {
        "name": "廖延俊",
        "key": "L"
      }
    ]
  },
  {
    title: "M",
    item: [{
        "name": "马小兵",
        "key": "M"
      },
      {
        "name": "梅龙杰",
        "key": "M"
      }
    ]
  },
  // {
  //   title: "N",
  //   item: [

  //     {
  //       "name": "南京",
  //       "key": "N"
  //     },
  //     {
  //       "name": "南昌",
  //       "key": "N"
  //     },
  //     {
  //       "name": "南宁",
  //       "key": "N"
  //     },
  //     {
  //       "name": "南充",
  //       "key": "N"
  //     }
  //   ]
  // },
  {
    title: "P",
    item: [

      {
        "name": "潘怡",
        "key": "P"
      },
      {
        "name": "潘婷婷",
        "key": "P"
      }
    ]
  },
  {
    title: "Q",
    item: [

      {
        "name": "秦姗姗",
        "key": "Q"
      },
      {
        "name": "齐霄超",
        "key": "Q"
      },
      {
        "name": "曲璐",
        "key": "Q"
      },
      {
        "name": "邱锦晖",
        "key": "Q"
      }
    ]
  },
  // {
  //   title: "R",
  //   item: [{
  //       "name": "日喀则",
  //       "key": "R"
  //     },
  //     {
  //       "name": "日照",
  //       "key": "R"
  //     }
  //   ]
  // },
  {
    title: "S",
    item: [{
        "name": "孙术明",
        "key": "S"
      },
      {
        "name": "沈珏辉",
        "key": "S"
      },
      {
        "name": "邵宇恒",
        "key": "S"
      },
      {
        "name": "施嘉璐",
        "key": "S"
      },
      {
        "name": "石雪砂",
        "key": "S"
      },
      {
        "name": "邵梦琳",
        "key": "S"
      }
    ]
  },
  // {
  //   title: "T",
  //   item: [

  //     {
  //       "name": "天津",
  //       "key": "T"
  //     },
  //     {
  //       "name": "太原",
  //       "key": "T"
  //     },
  //     {
  //       "name": "泰安",
  //       "key": "T"
  //     },
  //     {
  //       "name": "泰州",
  //       "key": "T"
  //     }
  //   ]
  // },
  {
    title: "W",
    item: [{
        "name": "温进鹏",
        "key": "W"
      },
      {
        "name": "王康",
        "key": "W"
      },
      {
        "name": "王昕成",
        "key": "W"
      },
      {
        "name": "王荣",
        "key": "W"
      },
      {
        "name": "王振涛",
        "key": "W"
      },
      {
        "name": "吴斌",
        "key": "W"
      },
      {
        "name": "吴浩",
        "key": "W"
      },
      {
        "name": "王亚丽",
        "key": "W"
      },
      {
        "name": "王兆华",
        "key": "W"

      },
      {
        "name": "王志婷",
        "key": "W"
      },
      {
        "name": "吴琦强",
        "key": "W"
      },
      {
        "name": "王潇阳",
        "key": "W"
      },
      {
        "name": "汪宇扬",
        "key": "W"
      },
      {
        "name": "吴滟泽",
        "key": "W"
      },
      {
        "name": "吴东辉",
        "key": "W"
      },

    ]
  },
  {
    title: "X",
    item: [

      {
        "name": "徐智强",
        "key": "X"
      },
      {
        "name": "谢和家",
        "key": "X"
      },
      {
        "name": "徐璇",
        "key": "X"
      },
      {
        "name": "荀浙",
        "key": "X"
      }
    ]
  },
  {
    title: "Y",
    item: [

      {
        "name": "姚超超",
        "key": "Y"
      },
      {
        "name": "姚斐",
        "key": "Y"
      },
      {
        "name": "姚莉钧",
        "key": "Y"
      },
      {
        "name": "叶瀚灿",
        "key": "Y"
      }
    ]
  },
  {
    title: "Z",
    item: [{
        "name": "张心怡",
        "key": "Z"
      },
      {
        "name": "诸铤",
        "key": "Z"
      },
      {
        "name": "张健涛",
        "key": "Z"
      },
      {
        "name": "周德意",
        "key": "Z"
      },
      {
        "name": "朱俊玮",
        "key": "Z"
      },
      {
        "name": "邹涵宇",
        "key": "Z"
      },
      {
        "name": "张娜娜",
        "key": "Z"
      },
      {
        "name": "张世纪",
        "key": "Z"
      },
      {
        "name": "周茜",
        "key": "Z"
      },
      {
        "name": "周大维",
        "key": "Z"
      },
      {
        "name": "周浩恩",
        "key": "Z"
      },
      {
        "name": "周李俊",
        "key": "Z"
      },
      {
        "name": "赵郑龑",
        "key": "Z"
      }
    ]
  }
]

module.exports = city;