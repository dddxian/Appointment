// miniprogram/pages/app1/app1.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    appointment: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  tiao: function (e) { // 带着id跳转
    wx.navigateTo({
      url: '../volunteer/task1?id=' + e.currentTarget.dataset.id
    })
  },
  onShow: function () {
    const db = wx.cloud.database()
    db.collection('volunteertask').orderBy('time', 'desc').get({
      success: res => {
        this.setData({
          appointment: res.data
        })
      },
      fail: err => {
        wx.showToast({
          icon: "none",
          title: '查询记录失败',
        })
      }
    })
  },
  onPullDownRefresh: function () {
    wx.showNavigationBarLoading()
    this.onLoad()
    wx.hideNavigationBarLoading();
  }

})