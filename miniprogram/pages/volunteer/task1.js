// miniprogram/pages/appointment-1/appointment-1.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    arry: [],
    id: '',
    ccc: [],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var id = options.id; /// 传递进来得id赋值给当前页面
    this.data.id = id;
  },
  updateit: function (e) {
    var that = this
    const send = e.detail.target.dataset.send;
    if (e.detail.value.serviceman.length == 0) {
      wx.showModal({
        title: '维修人员为空',
        content: '请输入您的名字！',
        showCancel: false,
        confirmColor: '#f00'
      })
    } else if (e.detail.value.serviceContact_way == 0) {
      wx.showModal({
        title: '维修人员联系方式为空',
        content: '请您的联系方式！',
        showCancel: false,
        confirmColor: '#f00'
      })
    } else {
      wx.showModal({
        title: '提示',
        content: '确定要修改人员信息吗？',
        success: function (res) {
          if (res.confirm) {
            console.log('点击确定了');
            wx.cloud.callFunction({
              // 云函数名称
              name: 'update',
              // 传给云函数的参数
              data: {
                id: that.data.id,
                status: '已派出',
                serviceman: e.detail.value.serviceman,
                serviceContact_way: e.detail.value.serviceContact_way,
              },
              success: function (res) {
                if (send == '1') {
                  wx.cloud.callFunction({
                    name: 'subsend',
                    data: {
                      sendid: that.data.id,
                      serviceContact_way: e.detail.value.serviceContact_way
                    }
                  })
                  wx.showToast({
                    icon: 'success',
                    title: '提交成功',
                  })
                }
                that.onShow()
                wx.redirectTo({
                  url: '../yapp1/app1',
                })
              },
              fail: function (res) {
                wx.showToast({
                  icon: 'none',
                  title: '提交失败',
                })
              }
            })
          } else if (res.cancel) {
            console.log('点击取消了');
            return false;
          }
        }
      })
    }
  },
  wancheng: function () { //已完成按钮的事件
    var that = this
    wx.showModal({
      title: '提示',
      content: '确认已完成吗？',
      success: function (res) {
        if (res.confirm) {
          console.log('点击确定了');
          wx.cloud.callFunction({
            // 云函数名称
            name: 'update',
            // 传给云函数的参数
            data: {
              id: that.data.id,
              status: '已完成',
            },
            success: function (res) {
              wx.showToast({
                icon: 'success',
                title: '提交成功',
              })
              that.onShow()
            },
            fail: function (res) {
              wx.showToast({
                icon: 'none',
                title: '提交失败',
              })
            }
          })
        } else if (res.cancel) {
          console.log('点击取消了');
          return false;
        }
      }
    })
  },
  fangda: function (e) { // 图片单机放大的函数
    var index = e.currentTarget.dataset.index;
    var imgArr = this.data.arry[0].images_fileID;
    wx.previewImage({
      current: imgArr[index], //当前图片地址
      urls: imgArr, //所有要预览的图片的地址集合 数组形式
      success: function (res) { },
      fail: function (res) { },
      complete: function (res) { },
    })
  },


  onShow: function () {
    const db = wx.cloud.database()
    // var id = options.currentTarget.id
    db.collection('volunteertask').where({
      _id: this.data.id
    }).get({
      success: res => {
        this.setData({
          arry: res.data,
          // ccc: res.data.images
        })
      },
      fail: err => {
        wx.showToast({
          icon: "none",
          title: '查询记录失败',
        })
      }
    })
  }

})