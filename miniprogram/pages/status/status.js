// miniprogram/pages/status/status.js
Page({
  /**
   * 页面的初始数据
   */
  data: {
    appointment: []
  },
  onShow: function(options) {
    wx.showToast({
      title: '加载中',
      icon: 'loading',
      duration: 500,
    })
    const app = getApp();
    const db = wx.cloud.database()
    if (app.globalData.openid){
    db.collection("appointment").where({
      _openid: app.globalData.openid
    }).orderBy('time', 'desc').get({ //根据时间倒序排列
      success: res => {
        this.setData({
          appointment: res.data
        })
      },
      fail: err => {
        wx.showToast({
          icon: "none",
          title: '查询记录失败',
        })
      }
    })
    }
    else{
      var that =this
      const app = getApp();
      // 调用云函数
      wx.cloud.callFunction({
        name: 'login', //函数名称
        data: {},
        success: res => {
          console.log('[云函数] [login] user openid: ', res.result.openid)
          app.globalData.openid = res.result.openid // 将openID记录在app.globalData里面，可以全局调用
          that.onShow()
        },
        fail: err => {
          console.error('[云函数] [login] 调用失败', err)

        }
      })
    }
  },
  onLoad: function(e) {
    var that = this
    const db = wx.cloud.database()
    db.collection('admin').where({
      _id: '3a3b90a1-c34f-4a7e-b92f-7b5fba007309'
    }).get({
      success: res => {
        that.setData({
          title1: res.data[0].title_app
        })
      },
      fail: err => {
        wx.showToast({
          icon: "none",
          title: '查询记录失败',
        })
      }
    })
  },
  cancel: function(e) {
    var that = this
    const db = wx.cloud.database()
    var id = e.currentTarget.dataset.id
    wx.showModal({
      title: '问问',
      content: '确定要取消预约吗？',
      success: function(res) {
        if (res.confirm) {
          console.log('点击确定了');
          db.collection('appointment').doc(id).remove({ //根据判断id进入数据库中删除该预约
            success: function(res) {
              wx.showToast({
                title: '取消成功',
              })
              that.onShow() //重新加载当前页面
            },
            fail: err => {
              wx.showToast({
                icon: 'none',
                title: '取消失败',
              })
              console.error('[数据库] [删除记录] 失败：', err)
            }
          })
        } else if (res.cancel) {
          console.log('点击取消了');
          return false;
        }
      }
    })

  },
  wancheng: function(e) { //已完成按钮的事件
    var that = this
    var id = e.currentTarget.dataset.id
    wx.showModal({
      title: '提示',
      content: '您的问题已解决？',
      success: function(res) {
        if (res.confirm) {
          console.log('点击确定了');
          wx.cloud.callFunction({
            // 云函数名称
            name: 'update',
            // 传给云函数的参数
            data: {
              id: id,
              status: '已完成',
            },
            success: function(res) {
              wx.showToast({
                icon: 'success',
                title: '提交成功',
              })
              that.onShow()
            },
            fail: function(res) {
              wx.showToast({
                icon: 'none',
                title: '提交失败',
              })
            }
          })
        } else if (res.cancel) {
          console.log('点击取消了');
          return false;
        }
      }
    })
  },
  finish: function() {
    wx.redirectTo({
      url: '../feedback/feedback?serviceman=' + this.data.appointment[0].serviceman
    })
  },
  onSubscribe: function(e) {
    if(e.currentTarget.dataset.send == 1){
      wx.showToast({
        title: '已订阅',
        icon:'none',
        duration:2000
      })
      return 0;
    }
    const db = wx.cloud.database()
    // 获取课程信息
    const id = e.currentTarget.dataset.id;
    const lessonTmplId = 'z2G9GAHvdW2Cg2cA7EFXyFSVuSKmRc_87we3ad5cw0s';
    // 调用微信 API 申请发送订阅消息
    wx.requestSubscribeMessage({
      // 传入订阅消息的模板id，模板 id 可在小程序管理后台申请
      tmplIds: [lessonTmplId],
      success(res) {
        // 申请订阅成功
        if (res.errMsg === 'requestSubscribeMessage:ok') {
          // 这里将订阅的课程信息调用云函数存入云开发数据
          wx.cloud.callFunction({
              name: 'subscribe',
              data: {
                templateId: lessonTmplId,
                sendid: id
              },
            })
            .then(() => {
                db.collection('appointment').doc(id).update({
                  // data 传入需要局部更新的数据
                  data: {
                    send: '1'
                  }
                })
                wx.showToast({
                  title: '订阅成功',
                  icon: 'success',
                  duration: 2000,
                });
            })
            .catch(() => {
              wx.showToast({
                title: '订阅失败',
                icon: 'none',
                duration: 2000,
              });
            });
        } else {
          //用户拒绝了订阅或当前游戏被禁用订阅消息
          wx.showToast({
            title: '订阅失败',
            icon: 'none'
          })
        }
      },
    });
  },
})