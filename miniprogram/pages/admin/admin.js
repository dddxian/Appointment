Page({
  data: {
    adminID: [],
    name: ''
  },

  onLoad: function () {
    // wx.cloud.database().collection('admin').where({
    //   _id: '83bd3f7a-03e9-4448-87f1-abea7497c3ed'
    // }).get({
    //   success: res => {
    //     console.log(res)
    //     this.setData({
    //       password: res.data[0].password
    //     })
    //     console.log(this.data.password)
    //   }
    // })
  },

  judge(e) {
    console.log(e.detail.value)
    // if (e.detail.value == this.data.password) {
    //   wx.redirectTo({
    //     url: '../task/task'
    //   })
    // }
    wx.cloud.callFunction({
      name: 'checklogin',
      data: {
        name: this.data.name,
        psw: e.detail.value
      },
      success: res => {
        console.log(res)
        if (res.result.isok) {
          wx.redirectTo({
            url: '../task/task'
          })
        }
      }
    })
  },
  input(e) {
    this.setData({
      name: e.detail.value
    })
  },
  compare: function (e) {
    if (e.detail.value.user.length == 0) {
      wx.showModal({
        title: '账号为空',
        content: '请输入账号！',
        showCancel: false,
        confirmColor: '#f00'
      })
    } else if (e.detail.value.user.key == 0) {
      wx.showModal({
        title: '密码为空',
        content: '请输入密码！',
        showCancel: false,
        confirmColor: '#f00'
      })
    } else {
      wx.showModal({
        title: '密码错误',
        content: '请重新输入密码！',
        showCancel: false,
        confirmColor: '#f00'
      })
    }
  }
})