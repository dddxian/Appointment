// pages/feed1/feed1.js
const db = wx.cloud.database()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // feedback:
    page: 0

  },

  onLoad: function(options) {
    // const app = getApp();
    db.collection('feedback').orderBy('time', 'desc').get({
      success: res => {
        this.jishu()
        console.log(this.data.page)
        this.setData({
          feedback: res.data
        })
      },
      fail: err => {
        wx.showToast({
          icon: "none",
          title: '查询记录失败',
        })
      }
    })
 
  },
  onShow:function(){
   
  },
  tiao: function(e) {
    // console.log(e.currentTarget.dataset.id)
    wx.navigateTo({
      url: '../feed2/feed2?id=' + e.currentTarget.dataset.id
    })
  },

  jishu: function() {
    let that = this
    // wx.cloud.callFunction({
    //   name: 'countf',
    //   data: {
    //     name: 'feedback'
    //   },
    //   success: function(res) {
    //     that.data.all = res.result.total
    //   }
    // })
    db.collection('feedback').count({
      success: function(res) {
        console.log(res)
        that.data.all = res.total
      }
    })

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    wx.showNavigationBarLoading()
    this.onLoad()
    wx.hideNavigationBarLoading();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    console.log(this.data.page)
    if (this.data.page <= this.data.all) {
      wx.hideNavigationBarLoading(); //隐藏加载
      wx.stopPullDownRefresh();
      this.load()
    } else {

    }
  },
  load: function() {
    let that = this
    let shu = 20
    let dir = this.data.feedback
    that.data.page = that.data.page + 20
    const db = wx.cloud.database()
    db.collection('feedback').orderBy('time', 'desc').skip(this.data.page).limit(shu).get({
      success: res => {
        dir = dir.concat(res.data)
        this.data.feedback = dir
        console.log(this.data.feedback)
        this.setData({
          feedback: this.data.feedback
        })
      },
      fail: err => {
        wx.showToast({
          icon: "none",
          title: '到底了哦~',
        })
      }
    })
  } 
})