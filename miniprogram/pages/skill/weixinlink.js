// miniprogram/pages/skill/weixinlink.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    src:''
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    const db = wx.cloud.database()
    db.collection('pages').doc(options.id).get({
      success:res=>{
        this.setData({
          src: res.data.src
        })
      }
    })
    
  },


})