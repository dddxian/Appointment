// miniprogram/pages/feed2/feed2.js
const db = wx.cloud.database()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    id:'',
    but:false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var id = options.id;
    this.data.id = id
    // this.data.id = options.id
    // var id = options.currentTarget.id
    db.collection('feedback').where({
      _id: id
    }).get({
      success: res => {
        this.setData({
          arry: res.data
        })
        if(res.data[0]._openid == getApp().globalData.openid){
          this.setData({
            but:true
          })
        }
      },
      fail: err => {
        wx.showToast({
          icon: "none",
          title: '查询记录失败',
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
 del(e){
   var id = this.data.arry[0]._id
  wx.showModal({
    title: '问问',
    content: '确定要删除反馈吗？',
    success: function(res) {
      if (res.confirm) {
        console.log('点击确定了');
        db.collection('feedback').doc(id).remove({
          success:res=>{
            wx.navigateBack({
            })
          }
        })
      } else if (res.cancel) {
        console.log('点击取消了');
        return false;
      }
    }
  })
  
 },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})