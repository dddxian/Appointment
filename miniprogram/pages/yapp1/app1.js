// miniprogram/pages/app1/app1.js
const db = wx.cloud.database()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    appointment: [],
    page: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    
  },
  tiao: function(e) {
    wx.navigateTo({
      url: '../appointment-1/appointment-1?id=' + e.currentTarget.dataset.id
    })
    // console.log(e)
  },

  /**
   * 生命周期函数--监听页面显示
   */
  jishu: function() {
    db.collection('feedback').count({
      success: function(res) {
        console.log(res)
        that.data.all = res.total
      }
    })
  },
  onShow: function() {
    
    db.collection('appointment').where({
      status: ('已派出'),
    }).orderBy('time', 'desc').get({
      success: res => {
        this.setData({
          appointment: res.data
        })
        this.jishu()
      },
      fail: err => {
        wx.showToast({
          icon: "none",
          title: '查询记录失败',
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    wx.showNavigationBarLoading()
    this.onLoad()
    wx.hideNavigationBarLoading();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    wx.showNavigationBarLoading()
    wx.hideNavigationBarLoading();
    wx.showToast({
      icon: "none",
      title: '已经到底了',
    })

  }
})