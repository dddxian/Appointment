Page({
  /**
   * 页面的初始数据
   */
  data: {
    a: 0,
    bottonClr: ['black', 'white', 'white', 'white', 'white', 'white', 'white', 'white'],
    setArr: [{
        id: '6',
        name: '维修反馈信息',
        img: '/images/fkb.png',
        margin: '30'
      },
      {
        id: '4',
        name: '加入我们',
        img: '/images/join.png',
        margin: '30'
      },
      {
        id: '5',
        name: '管理员入口',
        img: '/images/admin1.png',
        margin: '30'
      },
      // {
      //   id: '7',
      //   name: 'bug反馈及建议',
      //   img: '/images/liu.png',
      //   margin: '0'
      // }
    ]

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    // var adminID = that.data.adminID
    const db = wx.cloud.database();
    db.collection('admin').where({
      _id: '83bd3f7a-03e9-4448-87f1-abea7497c3ed'
    }).get({
      success: res => {
        // console.log(res)
        this.data.adminID = res.data[0].adminID
        // console.log(this.data.adminID)


      },
    })
  },
  tiaoguo: function() {
    const app = getApp();
    // console.log(this.data.adminID.length)
    for (var i = 0; i < this.data.adminID.length; i++) {
      // console.log(i)
      if (this.data.adminID[i] == app.globalData.openid)
        wx.navigateTo({
          url: '../task/task',
        })
      // return 0;
    }
    return 0
  },
  admin: function() {
    if (this.data.a == 0) {
      wx.navigateTo({
        url: '../admin/admin'
      })
    }
  },
  change(e) {
    var that = this;
    // console.log(e.target.id)
    let id = e.target.id
    if (e.target.id == "1") {
      wx.navigateTo({
        url: '../appointment/appointment'
      })
    }
    if (e.target.id == "2") {
      // that.onGetOpenid(),
      wx.navigateTo({
        url: '../status/status'
      })
    }
    if (e.target.id == "3") {
      wx.navigateTo({
        url: '../feedback/feedback'
      })
    }
    if (e.target.id == "4") {
      wx.navigateTo({
        url: '../joinus/join'
      })
    }
    if (e.target.id == "5") {
      const app = getApp();
      for (var i = 0; i < this.data.adminID.length; i++) {
        // console.log(i)
        if (this.data.adminID[i] == app.globalData.openid) {
          wx.navigateTo({
            url: '../task/task',
          })
          var a = 1;
          return 0;
        }
      }
      that.admin();

    }
    if (e.target.id == "6") {
      wx.navigateTo({
        url: '../feed1/feed1'
      })
    }
    if (e.target.id == "7") {
      wx.navigateTo({
        url: '../liuyan/liu'
      })
    }
  }
})