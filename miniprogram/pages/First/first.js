let interstitialAd = null
Page({
  /**
   * 页面的初始数据
   */
  data: {
    last: 0,
    title1: []
  },
  onLoad: function(options) { 
// 在页面onLoad回调事件中创建插屏广告实例
// if (wx.createInterstitialAd) {
//   interstitialAd = wx.createInterstitialAd({
//     adUnitId: 'adunit-fa987dffec142d17'
//   })
//   interstitialAd.onLoad(() => {})
//   interstitialAd.onError((err) => {})
//   interstitialAd.onClose(() => {})
// }

// // 在适合的场景显示插屏广告
// if (interstitialAd) {
//   interstitialAd.show().catch((err) => {
//     console.error(err)
//   })
// }
  },
  onShow: function(e) {
    var that = this
    const db = wx.cloud.database()
    db.collection('admin').where({
      _id: '3a3b90a1-c34f-4a7e-b92f-7b5fba007309'
    }).get({
      success: res => {
        that.setData({
          title1: res.data[0].title
        })
      },
      fail: err => {
        wx.showToast({
          icon: "none",
          title: '查询记录失败',
        })
      }
    })
  },
  onShareAppMessage: function() {

  },
  change(e) { //
    var now = Date.parse(new Date());
    var that = this;
    var time = now - that.data.last;
    let id = e.currentTarget.dataset.id;
    console.log(time);
    if (time > 1000) {
      if (id == "1") { // 根据判断id进入相应界面
        wx.getSetting({
          success: function(res) {
            if (res.authSetting['scope.userInfo']) {
              wx.getUserInfo({ //这边用来判断用户是否授权，有授权过就直接进入页面，没有授权则进入授权界面
                success: function(res) {
                  //从数据库获取用户信息
                  wx.navigateTo({
                    url: '../appointment/appointment'
                  })
                  //用户已经授权过
                },
              });
            } else {
              wx.navigateTo({
                url: '../shouquan/shouquan'
              })
            }
          }
        })
      }
      else if (id == "2") {
        wx.navigateTo({
          url: '../status/status' //跳转到预约状态页面 
        })
      }
      else if (id == "3") {
        wx.navigateTo({
          url: '../feedback/feedback' // 跳转到反馈填写界面
        })
      }else if (id == "4") {
        wx.navigateTo({
          url: '../skill/skillList' // 跳转到教程界面
        })
      } 
    }
    that.data.last = now;
  }
  
})