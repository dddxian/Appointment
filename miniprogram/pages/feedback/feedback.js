// miniprogram/pages/feedback/feedback.js

var wayIndex = -1;
var school_area = '';
var grade = '';
// 当联想词数量较多，使列表高度超过340rpx，那设置style的height属性为340rpx，小于340rpx的不设置height，由联想词列表自身填充
// 结合上面wxml的<scroll-view>
var arrayHeight = 0;



const app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    last: 0,
    min: 1, //最少字数
    max: 520,
    appointment: [],
    array: [
      // "信息与电子工程学院",
      // "萨塞克斯人工智能学院",
      // "工商管理学院",
      // "旅游与城乡规划学院",
      // "财务与会计学院",
      // "统计与数学学院",
      // "经济学院",
      // "金融学院",
      // "泰隆金融学院",
      // "食品与生物工程学院",
      // "环境科学与工程学院",
      // "计算机与信息工程学院",
      // "管理工程与电子商务学院",
      // "法学院、知识产权学院",
      // "人文与传播学院",
      // "公共管理学院",
      // "外国语学院",
      // "东方语言文化学院",
      // "艺术设计学院",
      // "马克思主义学院"
    ],
    address: '',
    serviceman: '', //点击结果项之后替换到文本框的值
    adapterSource: [], //匹配源
    bindSource: [], //绑定到页面的数据，根据用户输入动态变化
    hideScroll: true,
    index:'点击选择'

  },

  onLoad: function (options) {
    const db = wx.cloud.database()
    db.collection("admin").where({
      _id: '38508b97-4ba4-4821-b2db-2447fb1c3380'
    }).get({ 
      success: res => {
        console.log(res)
        this.setData({
          adapterSource: res.data[0].name,
          array:res.data[0].xueyuan,
          serviceman:options.serviceman
        })
      },
      fail: err => {
        wx.showToast({
          icon: "none",
          title: '查询记录失败',
        })
      }
    })
  },

  onReady: function() {
    console.log(app.globalData)
  },

  // onShow: function() {
  //   this.setData({
  //     serviceman: app.globalData.mydata.serviceman
  //   })
  // },

  // write1(e) {
  //   console.log("姓名：" + e.detail.value),
  //     app.globalData.mydata.name = e.detail.value

  // },
  write2: function(e) {
    console.log('picker发送选择改变，携带值为', e.detail.value, this.data.array[e.detail.value])
    this.setData({
      index: e.detail.value
    })
    // app.globalData.mydata.address = this.data.array[e.detail.value]
  },

  // select() {
  //   wx.navigateTo({
  //     url: '../../pages/select/select',
  //   })
  // },

  inputs: function(e) {
    // 获取输入框的内容
    var value = e.detail.value;
    // 获取输入框内容的长度
    var len = parseInt(value.length);

    this.setData({
      currentWordNumber: len //当前字数  
    });

  },
  shangchuan: function(e) {
    var college = this.data.array[e.detail.value.address];
    var time1 = Date.parse(new Date());
    var last = this.data.last;
    var time = time1 - last;
    if (time > 3000) {
      var date = new Date();
      var now = date.getFullYear() + "/" + ((date.getMonth() + 1) < "10" ? ("0" + (date.getMonth() + 1)) : (date.getMonth() + 1)) + "/" + (date.getDate() < "10" ? ("0" + date.getDate()) : date.getDate()) + " " + (date.getHours() < "10" ? ("0" + date.getHours()) : date.getHours()) + ":" + (date.getMinutes() < "10" ? ("0" + date.getMinutes()) : date.getMinutes()) + ":" + (date.getSeconds() < "10" ? ("0" + date.getSeconds()) : date.getSeconds());
      if (e.detail.value.name.length == 0) {
        wx.showModal({
          title: '姓名为空',
          content: '请输入您的名字！',
          showCancel: false,
          confirmColor: '#f00'
        })
      } else if (e.detail.value.address == null) {
        wx.showModal({
          title: '学院为空',
          content: '请选择您的学院！',
          showCancel: false,
          confirmColor: '#f00'
        })
      } else if (e.detail.value.serviceman == '点击选择') {
        wx.showModal({
          title: '维修人员未选择',
          content: '请选择！',
          showCancel: false,
          confirmColor: '#f00'
        })
      } else if (e.detail.value.solve.length == 0) {
        wx.showModal({
          title: '本次解决的问题未填写',
          content: '请填写本次解决的问题',
          showCancel: false,
          confirmColor: '#f00'
        })
      } else if (e.detail.value.opinion.length == 0) {
        wx.showModal({
          title: '本次服务的评价未填写',
          content: '请填写本次服务评价',
          showCancel: false,
          confirmColor: '#f00'
        })
      } else {
        const db = wx.cloud.database()
        console.log(e)
        db.collection('feedback').add({
          data: {
            name: e.detail.value.name,
            address: college,
            serviceman: e.detail.value.serviceman,
            manyidu: e.detail.value.manyidu,
            opinion: e.detail.value.opinion,
            solve: e.detail.value.solve,
            time: now
          },
          success: res => {
            // util.hideLoading()
            // 在返回结果中会包含新创建的记录的 _id
            this.setData({
              name: e.detail.value.name,
              address: college,
              serviceman: e.detail.value.serviceman,
              manyidu: e.detail.value.manyidu,
              opinion: e.detail.value.opinion,
              solve: e.detail.value.solve,
              time: now
            })
            wx.showToast({
              icon: 'none',
              title: '反馈提交成功',
              duration: 2000, //显示时长
            })
            console.log('[数据库] [新增记录] 成功，记录 _id: ', res._id),
              console.log(e),
              wx.redirectTo({
                url: '../feed1/feed1',
              })
          },
          fail: err => {
            // util.hideLoading()
            wx.showToast({
              icon: 'none',
              title: '反馈提交失败'
            })
            console.error('[数据库] [新增记录] 失败：', err)
          }
        })

      }
      this.data.last = time1;
    }
  },


  //当键盘输入时，触发input事件
  bindinput: function(e) {
    //用户实时输入值
    var prefix = e.detail.value
    //匹配的结果
    var newSource = []
    if (prefix != "") {
      // 对于数组array进行遍历，功能函数中的参数 `e`就是遍历时的数组元素值。
      this.data.adapterSource.forEach(function(e) {
        // 用户输入的字符串如果在数组中某个元素中出现，将该元素存到newSource中
        if (e.indexOf(prefix) != -1) {
          console.log(e);
          newSource.push(e)
        }
      })
    };
    // 如果匹配结果存在，那么将其返回，相反则返回空数组
    if (newSource.length != 0) {
      this.setData({
        // 匹配结果存在，显示自动联想词下拉列表
        hideScroll: false,
        bindSource: newSource,
        arrayHeight: newSource.length * 71
      })
    } else {
      this.setData({
        // 匹配无结果，不现实下拉列表
        hideScroll: true,
        bindSource: []
      })
    }
  },

  // 用户点击选择某个联想字符串时，获取该联想词，并清空提醒联想词数组
  itemtap: function(e) {
    this.setData({
      // .id在wxml中被赋值为{{item}}，即当前遍历的元素值
      serviceman: e.target.id,
      // 当用户选择某个联想词，隐藏下拉列表
      hideScroll: true,
      bindSource: []
    })
  },


})