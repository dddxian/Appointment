# 关于e预约

这是我暑假写的，目的是给我的社团使用。由于匆匆上手，很多命名不是很规范，遵循能跑就好的原则。
## 图片暂时不放了，后面再更新

### 部署方案
数据库集合创建以下几个（权限所有用户可读，仅创建者可读写）
- admin对应的是存储一些管理信息，公告，人员名单，具体在看代码中体会一下，自行定义也很容易
- appointment用于收集预约信息
- feedback用于收集反馈
- messages用于使用订阅消息发送
- pages用于存放公众号文章的标题和图片，主要内容是 list：文章优先级，越小越优先。name：文章名称，src：文章的公众号网址，imgsrc：封面图片打的地址

云函数需要部署login，subscribe，subsend，update，checklogin
#### 参考文档

- [云开发文档](https://developers.weixin.qq.com/miniprogram/dev/wxcloud/basis/getting-started.html)

