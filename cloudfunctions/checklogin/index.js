// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({
  // API 调用都保持和云函数当前所在环境一致
  env: cloud.DYNAMIC_CURRENT_ENV
})
const db = cloud.database()
const _ = db.command
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  var isok = false
  try {
    return await db.collection('admin').doc('83bd3f7a-03e9-4448-87f1-abea7497c3ed').get().then(res => {
      if (res.data.password == event.psw) {
        isok = true
      } else {}
    })
  } catch (e) {
    console.error(e)
  }
}