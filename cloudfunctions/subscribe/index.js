const cloud = require('wx-server-sdk');
cloud.init();
const db = cloud.database();

exports.main = async event => {
  try {
    const { OPENID } = cloud.getWXContext();
    // 在云开发数据库中存储用户订阅的信息
    const result = await db.collection('messages').add({
      data: {
        ...event,
        sendid:event.sendid,
        touser: OPENID,
        page: 'pages/status/status',
        done: false, // 消息发送状态设置为 false
      },
    });
    return result;
  } catch (err) {
    console.log(err);
    return err;
  }
};
